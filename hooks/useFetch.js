import { useState, useEffect } from "react";
import axios from "axios";

const useFetch = (url, method) => {
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);

    const options = {
        method: method,
        url: url,
    };

    const fetchData = async () => {
        setIsLoading(true);

        try {
            const response = await axios.request(options);

            setData(response.data);
            setIsLoading(false);
        } catch (error) {
            setError(error);
            alert('Something went wrong')
        } finally {
            setIsLoading(false);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return { data, isLoading, error };
}

export default useFetch;
