import axiosAuth from './axiosAuth';


const Client = {
 
  async getJob () {
    return axiosAuth.get('/vieclam');
  },
  async getDetailJob (id: number) {
    return axiosAuth.get(`/chi-tiet-viec-lam/${id}`);
  },
  async getFileApplication () {
    return axiosAuth.get('/hoso');
  },
  async getDetailFileApplication (id: number) {
    return axiosAuth.get(`/chi-tiet-ho-so/${id}`);
  },
  async getBlog () {
    return axiosAuth.get('/blog');
  },
  async getDetailBlog (id: number) {
    return axiosAuth.get(`/chi-tiet-blog/${id}`);
  },
  async searchJob (keyword: string) {
    return axiosAuth.get('/tim-kiem-viec-lam', {params: {keyword}});
  },
  async searchFileApplication (keyword: string) {
    return axiosAuth.get('/tim-kiem-ho-so', {params: {keyword}});
  },
  async searchBlog (keyword: string) {
    return axiosAuth.get('/tim-kiem-blog', {params: {keyword}});
  },
};

export default Client;