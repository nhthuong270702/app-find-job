import axios, { AxiosInstance } from "axios";
import StorageService from "../services/StorageService";

//console.log(publicRuntimeConfig.AUTH_API)
// Set up default config for http requests here
// Please have a look at here `https://github.com/axios/axios#request- config` for the full list of configs
const axiosAuth = axios.create({
  baseURL: "https://ptjobs.000webhostapp.com/api",
  headers: {
    "Accept": "application/json",
    "Content-Type": "application/x-www-form-urlencoded"
  },
});

createCheckLoginInterceptor(axiosAuth);
createConfigInterceptor(axiosAuth);

function createConfigInterceptor(ins: AxiosInstance) {
  ins.interceptors.request.use(
    (config) => {
      return config;
    },
    (error) => {
      throw error;
    }
  );
}

function createCheckLoginInterceptor(ins: AxiosInstance) {
  ins.interceptors.response.use(async (response) => {
    if (response.data.code === 401) {
      //ins.interceptors.response.eject(interceptor);
      StorageService.profile = undefined;
      StorageService.token = "";
      //createRefreshTokenInterceptor(ins);
    }
    return response;
  });
}

export default axiosAuth;
