import axiosAuth from './axiosAuth';

const Auth = {
 
  async register (data: any) {
    return axiosAuth.post('/register', data);
  },
  async login (data: any) {
    return axiosAuth.post('/login', data);
  },
  async getInfo (id: number) {
    return axiosAuth.get(`/thong-tin/${id}`);
  },
  async updateInfo (id: number, data: any) {
    return axiosAuth.post(`/cap-nhat-thong-tin/${id}`, data);
  },
  async changePassword (id: number, data: any) {
    return axiosAuth.post(`/doi-mat-khau/${id}`, data);
  },
};

export default Auth;