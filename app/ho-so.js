import { useState } from "react";
import { SafeAreaView, ScrollView, View } from "react-native";
import { Stack, useRouter } from "expo-router";

import { COLORS, icons, images, SIZES } from "../constants";
import { Welcome } from "../components";
import ScreenHeaderBtn from "../components/common/header/ScreenHeaderBtn";
import UngTuyen from "../components/ungtuyen/UngTuyen";

const HoSo = () => {
    const router = useRouter()
    const [searchTerm, setSearchTerm] = useState("");

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.lightWhite }}>
            <Stack.Screen
                options={{
                    headerStyle: { backgroundColor: COLORS.lightWhite },
                    headerShadowVisible: false,
                    headerLeft: () => (
                        <ScreenHeaderBtn iconUrl={icons.menu} dimension='60%' handlePress={() => router.push("/common")} />
                    ),
                    headerTitle: "Dannh sách hồ sơ",
                    headerTitleAlign: "center"
                }}
            />

            <ScrollView showsVerticalScrollIndicator={false}>
                <View
                    style={{
                        flex: 1,
                        padding: SIZES.medium,
                    }}
                >
                    <Welcome
                        searchTerm={searchTerm}
                        setSearchTerm={setSearchTerm}
                        handleClick={() => {
                            if (searchTerm) {
                                router.push(`/ho-so`)
                            }
                        }}
                    />
                    <UngTuyen />
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

export default HoSo;
