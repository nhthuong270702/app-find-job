import { useEffect, useState } from "react";
import { Alert, Button, SafeAreaView, ScrollView, Text, TextInput, View } from "react-native";
import { Stack, useRouter } from "expo-router";

import { COLORS, icons, SIZES } from "../constants";
import ScreenHeaderBtn from "../components/common/header/ScreenHeaderBtn";
import StorageService from "../services/StorageService";
import Auth from "../apis/Auth";

const CapNhatThongTin = () => {
    const router = useRouter()
    const [data, setData] = useState({
        email: "",
        name: "",
        oldPassword: "",
        newPassword: "",
        reNewPassword: "",
    });

    const handleChange = (text, field) => {
        setData({
            ...data,
            [field]: text,
        });
    };

    const onProfileChange = async () => {
        let data = {}
        if (StorageService.profile) {
            data = await StorageService.profile
            setData({
                ...data,
                name: JSON.parse(data)?.name,
                email: JSON.parse(data)?.email
            })
        } else {
            setProfile(null);
        }
    };

    useEffect(() => {
        StorageService.registerListener("re_profile", onProfileChange, {
            run1st: true,
        });
        return () => {
            StorageService.removeListener("re_profile", onProfileChange);
        };
    }, []);

    const handleUpdateInfo = async () => {
        if (!data.name) {
            Alert.alert("Vui lòng nhập tên");
            return;
        }
        let profileData = await StorageService.profile;

        try {
            const response = await Auth.updateInfo(JSON.parse(profileData)?.id, data);
            if (response.data.code === 1) {
                Alert.alert(response.data.message);
                StorageService.profile = {
                    ...JSON.parse(profileData),
                    name: data.name,
                };

            } else {
                Alert.alert(response.data.message);
            }
        } catch (error) {
            console.error("Network error: ", error);
            Alert.alert('Something went wrong. Please try again later.');
        }
    }

    const handleChangePassword = async () => {

        if (!data.oldPassword) {
            Alert.alert("Vui lòng nhập mật khẩu hiện tại");
            return;
        }
        if (!data.newPassword) {
            Alert.alert("Vui lòng nhập mật khẩu mới");
            return;
        }
        if (!data.reNewPassword) {
            Alert.alert("Vui lòng xác nhận mật khẩu");
            return;
        }
        if (data.newPassword !== data.reNewPassword) {
            Alert.alert("Mật khẩu không khớp");
            return;
        }

        if (data.newPassword.length < 8) {
            Alert.alert("Mật khẩu phải có ít nhất 8 ký tự");
            return;
        }
        let profileData = await StorageService.profile;
        try {
            const response = await Auth.changePassword(JSON.parse(profileData)?.id, data);
            if (response.data.code === 1) {
                Alert.alert(response.data.message);
            } else {
                Alert.alert(response.data.message);
            }
        } catch (error) {
            console.error("Network error: ", error);
            Alert.alert('Something went wrong. Please try again later.');
        }
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.lightWhite }}>
            <Stack.Screen
                options={{
                    headerStyle: { backgroundColor: COLORS.lightWhite },
                    headerShadowVisible: false,
                    headerLeft: () => (
                        <ScreenHeaderBtn iconUrl={icons.menu} dimension='60%' handlePress={() => router.push("/common")} />
                    ),
                    headerTitle: "Cập nhật thông tin",
                    headerTitleAlign: "center"
                }}
            />

            <ScrollView showsVerticalScrollIndicator={true}>
                <View
                    style={{
                        padding: SIZES.medium,
                    }}
                >

                    <Text style={{ fontSize: SIZES.medium, fontWeight: 600 }}>Email</Text>
                    <TextInput
                        value={data?.email}
                        editable={false}
                        style={{ marginBottom: SIZES.medium }}
                    />

                    <Text style={{ fontSize: SIZES.medium, fontWeight: 600 }}>Tên hiển thị</Text>
                    <TextInput
                        value={data?.name}
                        onChangeText={(text) => handleChange(text, "name")}
                        style={{ marginBottom: SIZES.medium }}
                    />
                    <Button title="Cập nhật" onPress={handleUpdateInfo}></Button>

                    <Text style={{ fontSize: SIZES.medium, fontWeight: 600, marginTop: SIZES.xxLarge }}>Mật khẩu hiện tại</Text>
                    <TextInput
                        placeholder="********"
                        value={data?.oldPassword}
                        onChangeText={(text) => handleChange(text, "oldPassword")}
                        style={{ marginBottom: SIZES.medium }}
                        secureTextEntry={true}
                        password={true}
                    />

                    <Text style={{ fontSize: SIZES.medium, fontWeight: 600 }}>Nhập mật khẩu mới</Text>
                    <TextInput
                        placeholder="********"
                        value={data?.newPassword}
                        onChangeText={(text) => handleChange(text, "newPassword")}
                        style={{ marginBottom: SIZES.medium }}
                        secureTextEntry={true}
                        password={true}
                    />

                    <Text style={{ fontSize: SIZES.medium, fontWeight: 600 }}>Xác nhận mật khẩu mới</Text>
                    <TextInput
                        placeholder="********"
                        value={data?.reNewPassword}
                        onChangeText={(text) => handleChange(text, "reNewPassword")}
                        style={{ marginBottom: SIZES.medium }}
                        secureTextEntry={true}
                        password={true}
                    />

                    <Button title="Đổi mật khẩu" onPress={handleChangePassword}></Button>

                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

export default CapNhatThongTin;
