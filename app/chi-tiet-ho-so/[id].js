import { Stack, useSearchParams } from "expo-router";
import { useCallback, useState, useEffect } from "react";
import {
    View,
    Text,
    SafeAreaView,
    ActivityIndicator,
    ScrollView
} from "react-native";

import { COLORS, SIZES } from "../../constants";
import JobAbout from "../../components/chi-tiet-ho-so/about/About";
import Company from "../../components/chi-tiet-ho-so/company/Company";
import Footer from "../../components/chi-tiet-ho-so/footer/Footer";
import Client from "../../apis/Client";

const JobDetails = () => {
    const params = useSearchParams();
    const [data, setData] = useState({});
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);

    const fetchData = async () => {
        setIsLoading(true);
        try {
            const response = await Client.getDetailFileApplication(params?.id);
            setData(response.data.data);
            setIsLoading(false);
        } catch (error) {
            setError(error);
            // alert('Something went wrong')
        }
    }

    useEffect(() => {
        fetchData();
    }, [params.id]);

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.lightWhite }}>
            <Stack.Screen
                options={{
                    headerStyle: { backgroundColor: COLORS.lightWhite },
                    headerShadowVisible: false,
                    headerTitle: `${data?.ten}`,
                }}
            />
            {isLoading ? (
                <ActivityIndicator size='large' color={COLORS.primary} />
            ) : error ? (
                <Text>Something went wrong</Text>
            ) : (
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{ padding: SIZES.medium, paddingBottom: 100 }}>
                        <Company
                            companyLogo={data?.anh}
                            sex={data?.gioitinh}
                            career={data?.nganhnghe}
                            companyName={data?.ten}
                            location={data?.diachi}
                        />

                        <JobAbout info={data ?? "No data provided"} />

                    </View>
                </ScrollView>
            )}
            <Footer data={data} url={'https://careers.google.com/jobs/results/'} type="hoso" />
        </SafeAreaView>
    );
};

export default JobDetails;
