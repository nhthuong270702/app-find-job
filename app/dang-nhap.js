import { SafeAreaView } from "react-native";
import { Stack, useRouter } from "expo-router";

import { COLORS } from "../constants";
import DangNhap from "../components/dang-nhap/DangNhap";

const DangNhap1 = () => {
    const router = useRouter()

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.lightWhite }}>
            <Stack.Screen
                options={{
                    headerShadowVisible: false,
                    headerTitle: "",
                    headerShown: false,
                }}
            />
            <DangNhap />
        </SafeAreaView>
    );
};

export default DangNhap1;
