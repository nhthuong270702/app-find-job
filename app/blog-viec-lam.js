import { useState } from "react";
import { SafeAreaView, ScrollView, View } from "react-native";
import { Stack, useRouter } from "expo-router";

import { COLORS, icons, images, SIZES } from "../constants";
import { Blog, Welcome } from "../components";
import ScreenHeaderBtn from "../components/common/header/ScreenHeaderBtn";

const BlogViecLam = () => {
    const router = useRouter()
    const [searchTerm, setSearchTerm] = useState("");

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.lightWhite }}>
            <Stack.Screen
                options={{
                    headerStyle: { backgroundColor: COLORS.lightWhite },
                    headerShadowVisible: false,
                    headerLeft: () => (
                        <ScreenHeaderBtn iconUrl={icons.menu} dimension='60%' handlePress={() => router.push("/common")} />
                    ),
                    headerTitle: "Các blog việc làm",
                    headerTitleAlign: "center"
                }}
            />

            <ScrollView showsVerticalScrollIndicator={false}>
                <View
                    style={{
                        flex: 1,
                        padding: SIZES.medium,
                    }}
                >
                    <Welcome
                        searchTerm={searchTerm}
                        setSearchTerm={setSearchTerm}
                        handleClick={() => {
                            if (searchTerm) {
                                router.push(`/viec-lam`)
                            }
                        }}
                    />
                    <Blog />
                    {/* <ViecLam /> */}
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

export default BlogViecLam;
