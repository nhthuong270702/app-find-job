import { SafeAreaView, ScrollView, View } from "react-native";
import { Stack, useRouter } from "expo-router";

import { COLORS } from "../constants";
import DangKy from "../components/dang-ky/DangKy";

const DangKy1 = () => {
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.lightWhite }}>
            <Stack.Screen
                options={{
                    headerShadowVisible: false,
                    headerTitle: "",
                    headerShown: false,
                }}
            />
            <DangKy />
        </SafeAreaView>
    );
};

export default DangKy1;
