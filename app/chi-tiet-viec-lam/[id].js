import { Stack, useRouter, useSearchParams } from "expo-router";
import { useCallback, useState, useEffect } from "react";
import {
    View,
    Text,
    SafeAreaView,
    ActivityIndicator,
    ScrollView
} from "react-native";

import {
    Company,
    JobAbout,
    JobFooter,

} from "../../components";
import { COLORS, SIZES } from "../../constants";
import Client from "../../apis/Client";

const JobDetails = () => {
    const params = useSearchParams();
    const [data, setData] = useState({});
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);

    const fetchData = async () => {
        setIsLoading(true);
        try {
            const response = await Client.getDetailJob(params?.id);
            setData(response.data.data);
            setIsLoading(false);
        } catch (error) {
            setError(error);
        }
    }

    useEffect(() => {
        fetchData();
    }, [params.id]);

    console.log(data);

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.lightWhite }}>
            <Stack.Screen
                options={{
                    headerStyle: { backgroundColor: COLORS.lightWhite },
                    headerShadowVisible: false,
                    headerTitle: `${data?.tieude}`,
                }}
            />
            {isLoading ? (
                <ActivityIndicator size='large' color={COLORS.primary} />
            ) : error ? (
                <Text>Something went wrong</Text>
            ) : (
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{ padding: SIZES.medium, paddingBottom: 100 }}>
                        <Company
                            companyLogo={data?.anh}
                            jobTitle={data?.tieude}
                            companyName={data?.tenquan}
                            location={data?.diachi}
                            createdDate={data?.created}
                            dueDate={data?.due}
                        />

                        <JobAbout info={data ?? "No data provided"} />
                    </View>
                </ScrollView>
            )}
            <JobFooter data={data} url={`https://ptjobs.000webhostapp.com/mail/${params.id}`} />
        </SafeAreaView>
    );
};

export default JobDetails;
