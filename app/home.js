import React from "react";
import { SafeAreaView, ScrollView, Text, View } from "react-native";
import { Stack, useRouter } from "expo-router";
import { COLORS, icons, images, SIZES } from "../constants";
import { SliderBox } from "react-native-image-slider-box";
import ScreenHeaderBtn from "../components/common/header/ScreenHeaderBtn";
import { useWindowDimensions } from 'react-native';
import ViecLam from "../components/home/vieclam/ViecLam";
import UngTuyen from "../components/home/ungtuyen/UngTuyen";
import Blog from "../components/home/blog/Blog";
import DanhGia from "../components/home/danhgia/DanhGia";

const SLIDE_IMAGES = [
    "https://ptjobs.000webhostapp.com/slider/2020032616062543.jpg",
    "https://ptjobs.000webhostapp.com/slider/tieu-chi-tuyen-nhan-vien-quan-ca-phe-cafe61.jpg",
    "https://ptjobs.000webhostapp.com/slider/part-time-la-gi4.jpg",
]

const Home = () => {
    const { width } = useWindowDimensions();
    const router = useRouter();

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.lightWhite }}>
            <Stack.Screen
                options={{
                    headerStyle: { backgroundColor: COLORS.lightWhite },
                    headerShadowVisible: false,
                    headerLeft: () => (
                        <ScreenHeaderBtn iconUrl={icons.menu} dimension='60%' handlePress={() => router.push("/common")} />
                    ),
                    headerTitle: "",
                }}
            />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View
                    style={{
                        flex: 1,
                        padding: SIZES.medium,
                    }}
                >
                    <SliderBox images={SLIDE_IMAGES} parentWidth={width} autoplay circleLoop />
                    <ViecLam />
                    <UngTuyen />
                    <Blog />
                    <DanhGia />
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

export default Home;
