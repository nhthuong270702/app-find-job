import { Stack, useRouter, useSearchParams } from "expo-router";
import { useCallback, useState, useEffect } from "react";
import {
    View,
    Text,
    SafeAreaView,
    ActivityIndicator,
    ScrollView
} from "react-native";

import JobAbout from "../../components/chi-tiet-blog/about/About";
import Company from "../../components/chi-tiet-blog/company/Company";
import { COLORS, SIZES } from "../../constants";
import axios from "axios";
import Client from "../../apis/Client";

const JobDetails = () => {
    const params = useSearchParams();
    const [data, setData] = useState({});
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);

    const fetchData = async () => {
        // setIsLoading(true);
        try {
            const response = await Client.getDetailBlog(params?.id);
            setData(response.data.data);
            setIsLoading(false);
        } catch (error) {
            setError(error);
            // alert('Something went wrong')
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.lightWhite }}>
            <Stack.Screen
                options={{
                    headerStyle: { backgroundColor: COLORS.lightWhite },
                    headerShadowVisible: false,
                    headerTitle: `${data?.tieude}`,
                }}
            />
            {isLoading ? (
                <ActivityIndicator size='large' color={COLORS.primary} />
            ) : error ? (
                <Text>Something went wrong</Text>
            ) : (
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{ padding: SIZES.medium }}>
                        <Company data={data} />

                        <JobAbout data={data ?? "No data provided"} />
                    </View>
                </ScrollView>
            )}
            {/* <JobFooter url={'https://careers.google.com/jobs/results/'} data={params.id} /> */}
        </SafeAreaView>
    );
};

export default JobDetails;
