import ScreenHeaderBtn from "./common/header/ScreenHeaderBtn";
import Sidebar from "./common/sidebar/sidebar";

// home screen
import Welcome from "./welcome/Welcome";
import ViecLam from "./vieclam/ViecLam";
import Hoso from "./ungtuyen/UngTuyen";
import Blog from "./blog/Blog";

// job details screen
import Company from "./chi-tiet-viec-lam/company/Company";
import { default as JobTabs } from "./chi-tiet-viec-lam/tabs/Tabs";
import { default as JobAbout } from "./chi-tiet-viec-lam/about/About";
import { default as JobFooter } from "./chi-tiet-viec-lam/footer/Footer";
import Specifics from "./chi-tiet-viec-lam/specifics/Specifics";

// common
import ViecLamCard from "./common/cards/ViecLam/ViecLamCard";
import UngTuyenCard from "./common/cards/UngTuyen/UngTuyenCard";
import BlogCard from "./common/cards/Blog/BlogCard";

export {
  ScreenHeaderBtn,
  Sidebar,
  Welcome,
  ViecLam,
  Hoso,
  Blog,
  Company,
  JobTabs,
  JobAbout,
  JobFooter,
  Specifics,
  ViecLamCard,
  UngTuyenCard,
  BlogCard
};
