import React, { useEffect, useState } from "react";
import { useRouter } from "expo-router";
import { View, TextInput, Alert, Text, TouchableOpacity } from "react-native";
import styles from "./DangKy.style";
import { ValidateEmail } from "../../utils/validateEmail";
import axios from "axios";
import Auth from "../../apis/Auth";

const validateEmail = (email) => {
    return String(email)
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
};
const DangKy = () => {
    const router = useRouter();
    const [data, setData] = useState({
        name: "",
        email: "",
        password: "",
        repassword: "",
    });

    const handleChange = (text, field) => {
        setData({
            ...data,
            [field]: text,
        });
    };

    const handleSubmit = async () => {
        if (!data.name) {
            Alert.alert("Vui lòng nhập họ tên");
            return;
        }
        if (!data.email) {
            Alert.alert("Vui lòng nhập email");
            return;
        }
        if (data.email && !validateEmail(data.email)) {
            Alert.alert("Email không hợp lệ");
            return;
        }
        if (!data.password) {
            Alert.alert("Vui lòng nhập mật khẩu");
            return;
        }
        if (!data.repassword) {
            Alert.alert("Vui lòng nhập lại mật khẩu");
            return;
        }
        if (data.password !== data.repassword) {
            Alert.alert("Mật khẩu không khớp");
            return;
        }

        if (data.password.length < 8) {
            Alert.alert("Mật khẩu phải có ít nhất 8 ký tự");
            return;
        }

        try {
            const response = await Auth.register(data);
            if (response.data.code === 1) {
                Alert.alert("Đăng ký thành công");
                router.push("/dang-nhap");
            } else {
                Alert.alert(response.data.message);
            }
        } catch (error) {
            console.error("Network error: ", error);
            Alert.alert('Something went wrong. Please try again later.');
        }

    }

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Đăng ký</Text>
            <Text style={styles.textLabel}>Họ tên</Text>
            <View style={styles.inputWrapper}>
                <TextInput
                    styles={styles.inputText}
                    placeholder="Nguyen Van A"
                    value={data.name}
                    onChangeText={(text) => handleChange(text, "name")}
                />
            </View>
            <Text style={styles.textLabel}>Email</Text>
            <View style={styles.inputWrapper}>
                <TextInput
                    styles={styles.inputText}
                    placeholder="abc@example.com"
                    value={data.email}
                    onChangeText={(text) => handleChange(text, "email")}
                />
            </View>
            <Text style={styles.textLabel}>Mật khẩu</Text>

            <View style={styles.inputWrapper}>
                <TextInput
                    styles={styles.inputText}
                    secureTextEntry={true}
                    password={true}
                    placeholder="********"
                    value={data.password}
                    onChangeText={(text) => handleChange(text, "password")}
                />
            </View>
            <Text style={styles.textLabel}>Nhập lại mật khẩu</Text>

            <View style={styles.inputWrapper}>
                <TextInput
                    styles={styles.inputText}
                    secureTextEntry={true}
                    password={true}
                    placeholder="********"
                    value={data.repassword}
                    onChangeText={(text) => handleChange(text, "repassword")}
                />
            </View>
            <TouchableOpacity
                style={styles.button}
                onPress={handleSubmit}
            >
                <Text style={{ color: "#fff", fontWeight: 600 }}>Đăng ký</Text>
            </TouchableOpacity>

            <Text style={styles.textBt} onPress={() => { router.push("/dang-nhap") }}>Tôi đã có tài khoản</Text>

        </View>
    );
};

export default DangKy;
