import React, { useEffect, useState } from "react";
import { Stack, useRouter } from "expo-router";
import { View, Text, Button, Linking } from "react-native";
import styles from "./CommonBox.style";
import { SafeAreaView } from "react-native-safe-area-context";
import { COLORS } from "../../constants";
import StorageService from "../../services/StorageService";

const CommonBox = () => {
    const router = useRouter();
    const [profile, setProfile] = useState(null);

    const onProfileChange = async () => {
        let data = {}
        if (StorageService.profile) {
            data = await StorageService.profile
            setProfile(JSON.parse(data));
        } else {
            setProfile(null);
        }
    };

    useEffect(() => {
        StorageService.registerListener("re_profile", onProfileChange, {
            run1st: true,
        });
        return () => {
            StorageService.removeListener("re_profile", onProfileChange);
        };
    }, []);

    const handleLogout = () => {
        StorageService.token = "";
        StorageService.profile = "";
        router.push('/home');
    }

    return (
        <SafeAreaView style={{ backgroundColor: COLORS.lightWhite }}>
            <Stack.Screen
                options={{
                    headerStyle: { backgroundColor: COLORS.lightWhite },
                    headerShadowVisible: false,
                    headerTitle: "",
                }}
            />
            <View style={styles.container}>
                {
                    profile &&
                    <View style={styles.viewName}>
                        <Text style={styles.text}>Xin chào,</Text>
                        <Text style={styles.name}>{profile?.name}</Text>
                        <Button title="Chỉnh sửa" onPress={() => router.push('cap-nhat-thong-tin')}></Button>
                    </View>
                }

                <View style={styles.item}>
                    <Text style={styles.text} onPress={() => router.push("/home")}>Trang chủ</Text>
                </View>
                <View style={styles.item}>
                    <Text style={styles.text} onPress={() => router.push("/viec-lam")}>Việc làm</Text>
                </View>
                <View style={styles.item}>
                    <Text style={styles.text} onPress={() => router.push("/ho-so")}>Hồ sơ</Text>
                </View>
                <View style={styles.item}>
                    <Text style={styles.text} onPress={() => router.push("/blog-viec-lam")}>Blog</Text>
                </View>
                <View style={styles.item}>
                    <Text style={styles.text} onPress={() => router.push("/viec-lam-yeu-thich")}>Việc làm yêu thích</Text>
                </View>
                <View style={styles.item}>
                    <Text style={styles.text} onPress={() => router.push("/ho-so-yeu-thich")}>Hồ sơ yêu thích</Text>
                </View>
                <View style={styles.item}>
                    <Text style={styles.text} onPress={() => Linking.openURL("https://ptjobs.000webhostapp.com/tintuyendung/tao-tin-tuyen-dung")}>Đăng tin tuyển dụng</Text>
                </View>
                <View style={styles.item}>
                    <Text style={styles.text} onPress={() => Linking.openURL("https://ptjobs.000webhostapp.com/tintimviec/tao-tin-tim-viec")}>Đăng hồ sơ xin việc</Text>
                </View>
                {
                    !profile
                        ?
                        <View style={styles.item}>
                            <Text style={styles.text} onPress={() => router.push("/dang-nhap")}>Đăng nhập</Text>
                        </View>
                        : <View style={styles.item}>
                            <Text style={styles.text} onPress={handleLogout}>Đăng xuất</Text>
                        </View>
                }
            </View>
        </SafeAreaView >
    );
};

export default CommonBox;
