import { StyleSheet } from "react-native";

import { COLORS, FONT, SIZES } from "../../constants";

const styles = StyleSheet.create({
  container: {
    marginTop: -10,
  },
  viewName: {
    marginBottom: SIZES.xxLarge,

  },
  name: {
    fontWeight: 600,
    marginLeft: SIZES.xSmall,
    fontSize: SIZES.large,
    marginBottom: SIZES.small,
  },
  item: {
    borderBottomColor: "#8B8B8B",
    borderBottomWidth: 0.5,
  },
  text: {
    marginLeft: SIZES.xSmall,
    marginTop: SIZES.xxSmall,
    marginBottom: SIZES.xxSmall,
    fontSize: SIZES.medium,
    fontWeight: 400,
  }
});

export default styles;
