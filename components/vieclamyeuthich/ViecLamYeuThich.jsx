import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, ActivityIndicator, TextInput, Image } from "react-native";
import styles from "./ViecLamYeuThich.style";
import { COLORS, icons } from "../../constants";
import ViecLamCard from "../common/cards/ViecLam/ViecLamCard";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useRouter } from "expo-router";

const ViecLamYeuThich = () => {
    const router = useRouter();
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [keyword, setKeyword] = useState('');

    const fetchData = async () => {
        setIsLoading(true);
        try {
            let storedData = await AsyncStorage.getItem('vieclam');
            if (storedData) {
                let parsedData = JSON.parse(storedData);
                setData(parsedData);
            }
            setIsLoading(false);
        } catch (error) {
            alert('Something went wrong');
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <View style={styles.container}>
            <View style={styles.searchContainer}>
                <View style={styles.searchWrapper}>
                    <TextInput
                        style={styles.searchInput}
                        placeholder="Nhập tiêu đề ,ngành nghề, địa chỉ"
                        value={keyword}
                        onChangeText={text => setKeyword(text)}
                    />
                </View>

                <TouchableOpacity style={styles.searchBtn}>
                    <Image
                        source={icons.search}
                        resizeMode="contain"
                        style={styles.searchBtnImage}
                    />
                </TouchableOpacity>
            </View>
            <View style={styles.header}>
                {/* <Text style={styles.headerTitle}>Việc làm yêu thích</Text> */}
            </View>

            <View style={styles.cardsContainer}>
                {isLoading ? (
                    <ActivityIndicator size='large' color={COLORS.primary} />
                ) : (

                    data && data.length > 0 ?
                        data?.map((job) => (
                            <ViecLamCard
                                job={job}
                                key={job.id}
                                handleNavigate={() => router.push(`/chi-tiet-viec-lam/${job.id}`)}
                            />
                        ))
                        :
                        <View>
                            <Text>Không tìm thấy kết quả</Text>
                        </View>
                )}
            </View>
        </View>
    );
};

export default ViecLamYeuThich;
