import React, { useState, useEffect } from 'react'
import { View, Text, TouchableOpacity, FlatList, ActivityIndicator } from 'react-native'
import { useRouter } from 'expo-router'

import styles from './UngTuyen.style'
import { COLORS, SIZES } from "../../../constants";
import UngTuyenCard from "../../common/cards/UngTuyen/UngTuyenCard";
import axios from "axios";


const UngTuyen = () => {
    const router = useRouter();
    const [isLoading, setIsLoading] = useState(false);
    const [data, setData] = useState([]);

    const fetchData = async () => {
        setIsLoading(true);
        try {
            const response = await axios.get('https://ptjobs.000webhostapp.com/api/home');
            setData(response.data.hosos);
            setIsLoading(false);
        } catch (error) {
            setError(error);
            alert('Something went wrong')
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.headerTitle}>Hồ sơ mới</Text>
                <TouchableOpacity>
                    <Text style={styles.headerBtn} onPress={() => { router.push(`/ho-so`) }}>Xem thêm</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.cardsContainer}>
                {isLoading ? (
                    <ActivityIndicator size="large" colors={COLORS.primary} />
                ) : (
                    data?.map((item) => (
                        <UngTuyenCard
                            item={item}
                            key={item.id}
                            handleNavigate={() => router.push(`/chi-tiet-ho-so/${item.id}`)}
                        />
                    ))
                )}
            </View>
        </View >
    )
}

export default UngTuyen
