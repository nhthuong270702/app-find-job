import React, { useState, useEffect } from 'react'
import { View, Text, ActivityIndicator, Image } from 'react-native'
import { useRouter } from 'expo-router'

import styles from './DanhGia.style'
import { COLORS, icons } from "../../../constants";
import axios from "axios";
import ViecLamCard from '../../common/cards/ViecLam/ViecLamCard';
import DanhGiaCard from '../../common/cards/DanhGia/DanhGiaCard';


const DanhGia = () => {
    const router = useRouter();
    const [isLoading, setIsLoading] = useState(false);
    const [data, setData] = useState([]);

    const fetchData = async () => {
        setIsLoading(true);
        try {
            const response = await axios.get('https://ptjobs.000webhostapp.com/api/home');
            setData(response.data.danhgias);
            setIsLoading(false);
        } catch (error) {
            setError(error);
            alert('Something went wrong')
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.headerTitle}>Đánh giá về PTJobs</Text>
            </View>

            <View style={styles.cardsContainer}>
                {isLoading ? (
                    <ActivityIndicator size='large' color={COLORS.primary} />
                ) : (
                    data?.map((job) => (
                        <DanhGiaCard
                            job={job}
                            key={job.id}
                        />
                    ))
                )}
            </View>
        </View>
    )
}

export default DanhGia
