import React, { useState, useEffect } from 'react'
import { View, Text, ActivityIndicator, TouchableOpacity } from 'react-native'
import { useRouter } from 'expo-router'

import styles from './ViecLam.style'
import { COLORS, icons } from "../../../constants";
import axios from "axios";
import ViecLamCard from '../../common/cards/ViecLam/ViecLamCard';


const ViecLam = () => {
    const router = useRouter();
    const [isLoading, setIsLoading] = useState(false);
    const [data, setData] = useState([]);

    const fetchData = async () => {
        setIsLoading(true);
        try {
            const response = await axios.get('https://ptjobs.000webhostapp.com/api/home');
            setData(response.data.vieclams);
            setIsLoading(false);
        } catch (error) {
            setError(error);
            alert('Something went wrong')
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.headerTitle}>Việc làm mới</Text>
                <TouchableOpacity>
                    <Text style={styles.headerBtn}
                        onPress={() => { router.push(`/viec-lam`) }}>Xem thêm</Text>
                </TouchableOpacity>
            </View>

            <View style={styles.cardsContainer}>
                {isLoading ? (
                    <ActivityIndicator size='large' color={COLORS.primary} />
                ) : (
                    data?.map((job) => (
                        <ViecLamCard
                            job={job}
                            key={job.id}
                            handleNavigate={() => router.push(`/chi-tiet-viec-lam/${job.id}`)}
                        />
                    ))
                )}
            </View>
        </View>
    )
}

export default ViecLam
