import React, { useEffect, useState } from "react";
import { useRouter } from "expo-router";
import { View, Text, TouchableOpacity, ActivityIndicator } from "react-native";
import styles from "./Blog.style";
import { COLORS } from "../../../constants";
import axios from "axios";
import BlogCard from "../../common/cards/Blog/BlogCard";

const Blog = () => {
    const router = useRouter();
    const [isLoading, setIsLoading] = useState(false);
    const [data, setData] = useState([]);

    const fetchData = async () => {
        setIsLoading(true);
        try {
            const response = await axios.get('https://ptjobs.000webhostapp.com/api/home');
            setData(response.data.blogs);
            setIsLoading(false);
        } catch (error) {
            setError(error);
            alert('Something went wrong')
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.headerTitle}>Các blog nổi bật</Text>
                <TouchableOpacity>
                    <Text style={styles.headerBtn}
                        onPress={() => { router.push(`/blog-viec-lam`) }}>Xem thêm</Text>
                </TouchableOpacity>
            </View>

            <View style={styles.cardsContainer}>
                {isLoading ? (
                    <ActivityIndicator size='large' color={COLORS.primary} />
                ) : (
                    data?.map((blog) => (
                        <BlogCard
                            blog={blog}
                            key={blog.id}
                            handleNavigate={() => router.push(`/chi-tiet-blog/${blog.id}`)}
                        />
                    ))
                )}
            </View>
        </View>
    );
};

export default Blog;
