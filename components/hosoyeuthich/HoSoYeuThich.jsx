import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, ActivityIndicator, TextInput, Image } from "react-native";
import { useNavigation } from "@react-navigation/native";
import styles from "./HoSoYeuThich.style";
import { COLORS, icons } from "../../constants";
import ViecLamCard from "../common/cards/ViecLam/ViecLamCard";
import AsyncStorage from "@react-native-async-storage/async-storage";
import UngTuyenCard from "../common/cards/UngTuyen/UngTuyenCard";
import { useRouter } from "expo-router";

const HoSoYeuThich = () => {
    const router = useRouter();
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [keyword, setKeyword] = useState('');

    const fetchData = async () => {
        setIsLoading(true);
        try {
            let storedData = await AsyncStorage.getItem('hoso');
            if (storedData) {
                let parsedData = JSON.parse(storedData);
                setData(parsedData);
            }
            setIsLoading(false);
        } catch (error) {
            alert('Something went wrong');
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <View style={styles.container}>
            <View style={styles.searchContainer}>
                <View style={styles.searchWrapper}>
                    <TextInput
                        style={styles.searchInput}
                        placeholder="Nhập tên, địa chỉ"
                        value={keyword}
                        onChangeText={text => setKeyword(text)}
                    />
                </View>

                <TouchableOpacity style={styles.searchBtn}>
                    <Image
                        source={icons.search}
                        resizeMode="contain"
                        style={styles.searchBtnImage}
                    />
                </TouchableOpacity>
            </View>
            <View style={styles.header}>
                {/* <Text style={styles.headerTitle}>Hồ sơ yêu thích</Text> */}
            </View>

            <View style={styles.cardsContainer}>
                {isLoading ? (
                    <ActivityIndicator size='large' color={COLORS.primary} />
                ) : (

                    data && data.length > 0 ?
                        data?.map((item) => (
                            <UngTuyenCard
                                item={item}
                                key={item.id}
                                handleNavigate={() => router.push(`/chi-tiet-ho-so/${item.id}`)}
                            />
                        ))
                        :
                        <View>
                            <Text>Không tìm thấy kết quả</Text>
                        </View>
                )}
            </View>
        </View>
    );
};

export default HoSoYeuThich;
