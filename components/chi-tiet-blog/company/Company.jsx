import React from 'react'
import { View, Text, Image } from 'react-native'
import styles from './company.style'

const Company = ({ data }) => {
    return (
        <View style={styles.container}>
            <View style={styles.logoBox}>
                <Image
                    source={{
                        uri: `https://ptjobs.000webhostapp.com/anh_blog/${data?.anh}`
                    }}
                    style={styles.logoImage}
                />
            </View>

            <View style={styles.jobTitleBox}>
                <Text style={styles.jobTitle}>{data?.tieude}</Text>
                <Text style={styles.desc}>
                    @{data?.tennguoiviet} {data?.luotxem ? "- " + data?.luotxem + " lượt xem" : ""}
                </Text>
            </View>
        </View>
    )
}

export default Company
