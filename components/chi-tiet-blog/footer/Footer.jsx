import { View, Text, TouchableOpacity, Image, Linking } from "react-native";

import styles from "./footer.style";
import { icons } from "../../../constants";
import AsyncStorage from '@react-native-async-storage/async-storage';

const saveData = async (key, value) => {
    try {
        await AsyncStorage.setItem(key, value);
        console.log('Data saved successfully');
    } catch (e) {
        console.log('Failed to save data');
    }
};

const getData = async (key) => {
    try {
        const value = await AsyncStorage.getItem(key);
        if (value !== null) {
            console.log('Data found:', value);
            return value;
        } else {
            console.log('Data not found');
        }
    } catch (e) {
        console.log('Failed to get data');
    }
};

const Footer = ({ url, data }) => {

    const handleClick = async () => {
        await saveData('data', getData('data') ? getData('data').push(data) : [data]);
    }

    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.likeBtn} onPress={handleClick}>
                <Image
                    source={icons.heartOutline}
                    resizeMode='contain'
                    style={styles.likeBtnImage}
                />
            </TouchableOpacity>

            <TouchableOpacity
                style={styles.applyBtn}
                onPress={() => Linking.openURL(url)}
            >
                <Text style={styles.applyBtnText}>Ứng tuyển ngay</Text>
            </TouchableOpacity>
        </View>
    );
};

export default Footer;
