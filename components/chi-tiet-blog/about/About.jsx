import React from 'react'
import { View, Text, Image } from 'react-native'
import HTML from 'react-native-render-html';
import styles from './about.style';
import { useWindowDimensions } from 'react-native';

const JobAbout = ({ data }) => {

    const { width } = useWindowDimensions();

    return (
        <View style={styles.container}>
            <View style={styles.contentBox}>
                <HTML contentWidth={width} source={{ html: data?.noidung }} />
            </View>
        </View>
    )
}

export default JobAbout
