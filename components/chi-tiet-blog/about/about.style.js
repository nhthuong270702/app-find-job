import { StyleSheet } from "react-native";

import { COLORS, FONT, SIZES } from "../../../constants";

const styles = StyleSheet.create({
  container: {
    marginTop: SIZES.medium,
    backgroundColor: "#FFF",
    borderRadius: SIZES.small,
    padding: SIZES.small,
  },
  contentBox: {
    marginVertical: SIZES.small,
  },
  contextText: {
    fontSize: SIZES.medium - 2,
    color: COLORS.gray,
    fontFamily: FONT.regular,
    width: "100%",
    marginVertical: SIZES.small / 1.25,
  },
});

export default styles;
