import React from 'react'
import { View, Text, Image } from 'react-native'
import HTML from 'react-native-render-html';
import styles from './about.style'
import { format } from 'date-fns';

const JobAbout = ({ info }) => {

    return (
        <View style={styles.container}>
            <Text style={styles.headText}>Số lượng:</Text>
            <View style={styles.contentBox}>
                <Text style={styles.contextText}>{info?.soluong}</Text>
            </View>

            <Text style={styles.headText}>Lương: </Text>
            <View style={styles.contentBox}>
                <Text style={styles.contextText}>{info?.luong}</Text>
            </View>

            <Text style={styles.headText}>Thời gian làm việc: </Text>
            <View style={styles.contentBox}>
                <Text style={styles.contextText}>{info?.thoigian}</Text>
            </View>

            <Text style={styles.headText}>Ngày đăng: </Text>
            <View style={styles.contentBox}>
                <Text style={styles.contextText}>{info?.created_at ? format(new Date(info.created_at), 'dd-MM-yyyy') : null}</Text>
            </View>

            <Text style={styles.headText}>Ngày hết hạn ứng tuyển: </Text>
            <View style={styles.contentBox}>
                <Text style={styles.contextText}>{info?.ngayhethan ? format(new Date(info.ngayhethan), 'dd-MM-yyyy') : null}</Text>
            </View>

            <Text style={styles.headText}>Thời gian làm việc: </Text>
            <View style={styles.contentBox}>
                <Text style={styles.contextText}>{info?.thoigian}</Text>
            </View>

            <Text style={styles.headText}>Mô tả công việc:</Text>

            <View style={styles.contentBox}>
                <Text style={styles.contextText}><HTML source={{ html: info?.mota }} /></Text>
            </View>
        </View>
    )
}

export default JobAbout
