import React, { useState, useEffect } from 'react'
import { View, Text, TouchableOpacity, ActivityIndicator, TextInput, Image } from "react-native";
import { useRouter } from 'expo-router'
import styles from './UngTuyen.style'
import { COLORS, icons } from "../../constants";
import UngTuyenCard from "../common/cards/UngTuyen/UngTuyenCard";
import axios from "axios";
import Client from '../../apis/Client';


const UngTuyen = () => {
    const router = useRouter();
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [keyword, setKeyword] = useState('');

    const fetchData = async () => {
        setIsLoading(true);
        try {
            const response = await Client.getFileApplication();
            setData(response.data.data);
            setIsLoading(false);
        } catch (error) {
            // setError(error);
            alert('Something went wrong')
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSearch = async () => {
        setKeyword("");
        try {
            const response = await Client.searchFileApplication(keyword);
            if (response.data.status === "success") {
                setData(response.data.data);
            } else {
                fetchData();
            }
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <View style={styles.container}>
            <View style={styles.searchContainer}>
                <View style={styles.searchWrapper}>
                    <TextInput
                        styles={styles.searchInput}
                        placeholder="Nhập tên / ngành nghề / địa chỉ"
                        value={keyword}
                        onChangeText={text => setKeyword(text)}
                    />
                </View>

                <TouchableOpacity style={styles.searchBtn} onPress={handleSearch}>
                    <Image
                        source={icons.search}
                        resizeMode="contain"
                        style={styles.searchBtnImage}
                    />
                </TouchableOpacity>
            </View>
            <View style={styles.header}>
                {/* <Text style={styles.headerTitle}>Hồ sơ mới</Text> */}
                {/* <TouchableOpacity>
                    <Text style={styles.headerBtn}>Xem thêm</Text>
                </TouchableOpacity> */}
            </View>

            <View style={styles.cardsContainer}>
                {isLoading ? (
                    <ActivityIndicator size="large" colors={COLORS.primary} />
                ) : (
                    data?.map((item) => (
                        <UngTuyenCard
                            item={item}
                            key={item.id}
                            handleNavigate={() => router.push(`/chi-tiet-ho-so/${item.id}`)}
                        />
                    ))
                )}
            </View>
        </View >
    )
}

export default UngTuyen
