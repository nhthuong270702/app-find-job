import { StyleSheet } from "react-native";

import { COLORS, FONT, SIZES } from "../../constants";

const styles = StyleSheet.create({
  container: {
    marginTop: "auto",
    marginBottom: "auto",
  },
  title: {
    marginLeft: "auto",
    marginRight: "auto",
    marginBottom: SIZES.large,
    fontSize: SIZES.large,
    fontWeight: 700
  },
  textLabel: {
    marginLeft: SIZES.medium,
    marginRight: SIZES.medium,
    marginTop: SIZES.xxLarge,
    marginBottom: SIZES.xxSmall,
  },
  inputWrapper: {
    backgroundColor: COLORS.white,
    borderRadius: SIZES.medium,
    padding: SIZES.small,
    marginLeft: SIZES.medium,
    marginRight: SIZES.medium
  },
  inputText: {
    backgroundColor: "black",
  },
  button: {
    backgroundColor: "#0B0E55",
    borderRadius: SIZES.medium,
    padding: SIZES.small,
    marginLeft: SIZES.medium,
    marginRight: SIZES.medium,
    justifyContent: "center",
    alignItems: "center",
    marginTop: SIZES.xxLarge,
  },
  textBt: {
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: SIZES.medium,
  }
});

export default styles;
