import React, { useEffect, useState } from "react";
import { useRouter } from "expo-router";
import { View, TextInput, Alert, Text, TouchableOpacity } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import styles from "./DangNhap.style";
import axios from "axios";
import Auth from "../../apis/Auth";
import StorageService from "../../services/StorageService";

const validateEmail = (email) => {
    return String(email)
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
};

const DangNhap = () => {
    const router = useRouter();
    const [data, setData] = useState({
        email: "",
        password: "",
    });

    const handleChange = (text, field) => {
        setData({
            ...data,
            [field]: text,
        });
    };

    const handleSubmit = async () => {
        if (!data.email) {
            Alert.alert("Vui lòng nhập email");
            return;
        }
        if (data.email && !validateEmail(data.email)) {
            Alert.alert("Email không hợp lệ");
            return;
        }
        if (!data.password) {
            Alert.alert("Vui lòng nhập mật khẩu");
            return;
        }

        if (data.password.length < 8) {
            Alert.alert("Mật khẩu phải có ít nhất 8 ký tự");
            return;
        }

        try {
            const response = await Auth.login(data);
            if (response.data.code === 1) {
                StorageService.token = response.data.token;
                StorageService.profile = response.data.user[0]
                Alert.alert(response.data.message);
                router.push("/home");
            } else {
                Alert.alert(response.data.message);
            }
        } catch (error) {
            console.error("Network error: ", error);
            Alert.alert('Something went wrong. Please try again later.');
        }

    }

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Đăng nhập</Text>
            <Text style={styles.textLabel}>Email</Text>
            <View style={styles.inputWrapper}>
                <TextInput
                    styles={styles.inputText}
                    placeholder="abc@example.com"
                    value={data.email}
                    onChangeText={(text) => handleChange(text, "email")}
                />
            </View>
            <Text style={styles.textLabel}>Mật khẩu</Text>

            <View style={styles.inputWrapper}>
                <TextInput
                    styles={styles.inputText}
                    secureTextEntry={true}
                    password={true}
                    placeholder="********"
                    value={data.password}
                    onChangeText={(text) => handleChange(text, "password")}
                />
            </View>
            <TouchableOpacity
                style={styles.button}
                onPress={handleSubmit}
            >

                <Text style={{ color: "#fff", fontWeight: 600 }} >Đăng nhập</Text>
            </TouchableOpacity>

            <Text style={styles.textBt} onPress={() => { router.push("/home") }}>Về trang chủ</Text>
            <Text style={styles.textBt} onPress={() => { router.push("/dang-ky") }}>Đăng kí tài khoản mới</Text>
        </View>
    );
};

export default DangNhap;
