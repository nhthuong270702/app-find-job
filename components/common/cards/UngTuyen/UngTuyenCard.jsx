import React from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'

import styles from './UngTuyenCard.style'

const UngTuyenCard = ({ item, handleNavigate }) => {
    return (
        <TouchableOpacity
            style={styles.container(item)}
            onPress={handleNavigate}
        >
            <TouchableOpacity style={styles.logoContainer(item)}>
                <Image
                    source={{
                        uri: `https://ptjobs.000webhostapp.com/anh_tintimviec/${item.anh}`
                    }}
                    resizeMode="contain"
                    style={styles.logoImage}
                />
            </TouchableOpacity>
            <Text style={styles.companyName} numberOfLines={1}>{item.nganhnghe}</Text>

            <View style={styles.infoContainer}>
                <Text>
                    {item.ten}
                </Text>
                <Text style={styles.location}>
                    {item.diachi}
                </Text>
            </View>
        </TouchableOpacity>
    )
}

export default UngTuyenCard
