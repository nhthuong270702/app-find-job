import { StyleSheet } from "react-native";

import { COLORS, FONT, SHADOWS, SIZES } from "../../../../constants";

const styles = StyleSheet.create({
  container: () => ({
    width: "47%",
    padding: SIZES.small,
    backgroundColor: "#FFF",
    borderRadius: SIZES.medium,
    ...SHADOWS.medium,
    shadowColor: COLORS.white,
    alignItems: "center",
  }),
  logoContainer: () => ({
    width: 100,
    height: 100,
    backgroundColor: COLORS.white,
    borderRadius: SIZES.medium,
  }),
  logoImage: {
    width: "100%",
    height: "100%",

  },
  companyName: {
    fontSize: SIZES.medium,
    fontFamily: FONT.regular,
    color: "#B3AEC6",
    marginTop: SIZES.small / 1.5,

  },
  infoContainer: {
    marginTop: SIZES.large,
  },
  jobName: () => ({
    width: 100,
    height: 100,
    fontSize: SIZES.large,
    fontFamily: FONT.medium,
    color: COLORS.primary,
  }),
  infoWrapper: {
    flexDirection: "row",
    marginTop: 5,
    justifyContent: "flex-start",
    alignItems: "center",
  },
  publisher: () => ({
    fontSize: SIZES.medium - 2,
    fontFamily: FONT.bold,
    color: COLORS.primary,
  }),
  location: {
    fontSize: SIZES.medium - 2,
    fontFamily: FONT.regular,
    color: "#B3AEC6",
  },
});

export default styles;
