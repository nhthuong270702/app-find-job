import React from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'

import styles from './ViecLamCard.style'

import { checkImageURL } from "../../../../utils";

const ViecLamCard = ({ job, handleNavigate }) => {
    console.log(job);
    return (
        <TouchableOpacity
            style={styles.container}
            onPress={handleNavigate}
        >
            <TouchableOpacity style={styles.logoContainer}>
                <Image
                    source={{
                        uri: `https://ptjobs.000webhostapp.com/anh_tintuyendung/${job.anh}`
                    }}
                    resizeMode="contain"
                    style={styles.logoImage}
                />
            </TouchableOpacity>
            <View style={styles.textContainer}>
                <Text style={styles.jobName} numberOfLines={1}>
                    {job.tieude}
                </Text>
                <Text style={styles.jobType}>
                    {job.diachi}
                </Text>
            </View>
        </TouchableOpacity>
    )
}

export default ViecLamCard
