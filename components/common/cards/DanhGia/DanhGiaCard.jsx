import React from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'

import styles from './DanhGiaCard.style'

import { checkImageURL } from "../../../../utils";

const DanhGiaCard = ({ job, handleNavigate }) => {
    return (
        <TouchableOpacity
            style={styles.container}
            onPress={handleNavigate}
        >
            <TouchableOpacity style={styles.logoContainer}>
                <Image
                    source={{
                        uri: 'https://w7.pngwing.com/pngs/205/731/png-transparent-default-avatar-thumbnail.png'
                    }}
                    resizeMode="contain"
                    style={styles.logoImage}
                />
            </TouchableOpacity>
            <View style={styles.textContainer}>
                <Text style={styles.jobName} numberOfLines={1}>
                    {job.ten}
                </Text>
                <Text style={styles.jobType}>
                    {job.noidung}
                </Text>
            </View>
        </TouchableOpacity>
    )
}

export default DanhGiaCard
