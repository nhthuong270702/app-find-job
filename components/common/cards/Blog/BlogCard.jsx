import React from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'

import styles from './BlogCard.style'

const BlogCard = ({ blog, handleNavigate }) => {
    return (
        <TouchableOpacity
            style={styles.container}
            onPress={handleNavigate}
        >
            <TouchableOpacity style={styles.logoContainer}>
                <Image
                    source={{
                        uri: `https://ptjobs.000webhostapp.com/anh_blog/${blog.anh}`
                    }}
                    resizeMode="contain"
                    style={styles.logoImage}
                />
            </TouchableOpacity>
            <View style={styles.textContainer}>
                <Text style={styles.jobName}>
                    {blog.tieude}
                </Text>
                <Text style={styles.jobType}>
                    @{blog.tennguoiviet} {blog.luotxem ? "- " + blog.luotxem + " lượt xem" : ""}
                </Text>
            </View>
        </TouchableOpacity>
    )
}

export default BlogCard;
