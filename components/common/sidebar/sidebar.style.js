import { StyleSheet } from "react-native";
import { COLORS, SIZES } from "../../../constants";


const styles = StyleSheet.create({
  // Existing styles

  sidebarContainer: {
    flex: 1,
    backgroundColor: COLORS.white,
    padding: 20,
  },
  sidebarText: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
  },
});

export default styles;