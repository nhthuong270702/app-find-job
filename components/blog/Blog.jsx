import React, { useEffect, useState } from "react";
import { useRouter } from "expo-router";
import { View, Text, TouchableOpacity, ActivityIndicator, TextInput, Image } from "react-native";
import styles from "./Blog.style";
import { COLORS, icons } from "../../constants";
import axios from "axios";
import BlogCard from "../common/cards/Blog/BlogCard";
import Client from "../../apis/Client";

const Blog = () => {
    const router = useRouter();
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [keyword, setKeyword] = useState('');

    const fetchData = async () => {
        setIsLoading(true);
        try {
            const response = await Client.getBlog();
            setData(response.data.data);
            setIsLoading(false);
        } catch (error) {
            alert('Something went wrong')
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSearch = async () => {
        setKeyword("");
        try {
            const response = await Client.searchBlog(keyword);
            if (response.data.status === "success") {
                setData(response.data.data);
            } else {
                fetchData();
            }
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <View style={styles.container}>
            <View style={styles.searchContainer}>
                <View style={styles.searchWrapper}>
                    <TextInput
                        styles={styles.searchInput}
                        placeholder="Nhập tên người viết / tiêu đề"
                        value={keyword}
                        onChangeText={text => setKeyword(text)}
                    />
                </View>

                <TouchableOpacity style={styles.searchBtn} onPress={handleSearch}>
                    <Image
                        source={icons.search}
                        resizeMode="contain"
                        style={styles.searchBtnImage}
                    />
                </TouchableOpacity>
            </View>
            <View style={styles.header}>
                {/* <Text style={styles.headerTitle}>Các blog nổi bật</Text> */}
                {/* <TouchableOpacity>
                    <Text style={styles.headerBtn}
                        onPress={() => { router.push(`/blog`) }}>Xem thêm</Text>
                </TouchableOpacity> */}
            </View>

            <View style={styles.cardsContainer}>
                {isLoading ? (
                    <ActivityIndicator size='large' color={COLORS.primary} />
                ) : (
                    data?.map((blog) => (
                        <BlogCard
                            blog={blog}
                            key={blog.id}
                            handleNavigate={() => router.push(`/chi-tiet-blog/${blog.id}`)}
                        />
                    ))
                )}
            </View>
        </View>
    );
};

export default Blog;
