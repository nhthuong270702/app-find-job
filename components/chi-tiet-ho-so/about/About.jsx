import React from 'react'
import { View, Text, ScrollView, SafeAreaView } from 'react-native'
import HTML from 'react-native-render-html';
import styles from './about.style'
import { format } from 'date-fns';

const JobAbout = ({ info }) => {

    return (

        <View style={styles.container}>
            <Text style={styles.headText}>Ngày sinh:</Text>
            <View style={styles.contentBox}>
                <Text style={styles.contextText}>{info?.ngaysinh}</Text>
            </View>

            <Text style={styles.headText}>Số điện thoại: </Text>
            <View style={styles.contentBox}>
                <Text style={styles.contextText}>{info?.sdt}</Text>
            </View>

            <Text style={styles.headText}>Email: </Text>
            <View style={styles.contentBox}>
                <Text style={styles.contextText}>{info?.email}</Text>
            </View>
            <Text style={styles.headText}>Ngày đăng: </Text>
            <View style={styles.contentBox}>
                <Text style={styles.contextText}>{info?.created_at ? format(new Date(info.created_at), 'dd-MM-yyyy') : null}</Text>
            </View>
            <Text style={styles.headText}>Mô tả bản thân:</Text>

            <View style={styles.contentBox}>
                <Text style={styles.contextText}><HTML source={{ html: info?.mota }} /></Text>
            </View>
        </View>
    )
}

export default JobAbout
