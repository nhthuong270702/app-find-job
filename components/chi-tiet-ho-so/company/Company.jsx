import React from 'react'
import { View, Text, Image } from 'react-native'
import styles from './company.style'
import { icons } from "../../../constants";
import { checkImageURL } from '../../../utils';

const Company = ({ companyLogo, sex, companyName, location, career }) => {
    return (
        <View style={styles.container}>
            <View style={styles.logoBox}>
                <Image
                    source={{
                        uri: `https://ptjobs.000webhostapp.com/anh_tintimviec/${companyLogo}`
                    }}
                    style={styles.logoImage}
                />
            </View>

            <View style={styles.jobTitleBox}>
                <Text style={styles.jobTitle}>{career}</Text>
            </View>

            <View style={styles.companyInfoBox}>
                <Text style={styles.companyName}>
                    {companyName} - {sex}
                </Text>
                <View style={styles.locationBox}>
                    <Image
                        source={icons.location}
                        resizeMode="contain"
                        style={styles.locationImage}
                    />
                    <Text style={styles.locationName}>{location}</Text>
                </View>
            </View>
        </View>
    )
}

export default Company
