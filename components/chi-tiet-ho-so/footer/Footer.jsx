import { View, Text, TouchableOpacity, Image, Linking, Alert } from "react-native";

import styles from "./footer.style";
import { icons } from "../../../constants";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useEffect, useState } from "react";

const saveData = async (key, value) => {
    try {
        let existingData = await AsyncStorage.getItem(key);
        let newData = [];
        if (existingData) {
            newData = JSON.parse(existingData);
            // Kiểm tra xem dữ liệu có tồn tại trong mảng không
            const index = newData.findIndex(item => item.id === value.id);
            if (index === -1) {
                newData.push(value);
                Alert.alert('Đã thêm vào việc làm yêu thích');

            } else {
                newData.splice(index, 1);
                Alert.alert('Đã xóa việc làm này khỏi việc làm yêu thích');

            }
        } else {
            newData.push(value);
        }
        await AsyncStorage.setItem(key, JSON.stringify(newData));
    } catch (e) {
        Alert.alert('Thêm thất bại');
    }
};

const Footer = ({ url, data }) => {

    const [isLiked, setIsLiked] = useState(false);

    const handleClick = async () => {
        await saveData('hoso', data);
        setIsLiked(!isLiked);
    };

    useEffect(() => {
        const checkLikedStatus = async () => {
            let existingData = await AsyncStorage.getItem('hoso');
            if (existingData) {
                const newData = JSON.parse(existingData);
                const index = newData.findIndex(item => item.id === data.id);
                if (index !== -1) {
                    setIsLiked(true);
                }
            }
        };
        checkLikedStatus();
    }, [data.id]);

    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.likeBtn} onPress={handleClick}>
                <Image
                    source={isLiked ? icons.heart : icons.heartOutline}
                    resizeMode='contain'
                    style={styles.likeBtnImage}
                />
            </TouchableOpacity>


            <TouchableOpacity
                style={styles.applyBtn}
                onPress={() => Linking.openURL(`tel:${data?.sdt}`)}
            >
                <Text style={styles.applyBtnText}>Liên hệ ngay</Text>
            </TouchableOpacity>
        </View>
    );
};

export default Footer;
