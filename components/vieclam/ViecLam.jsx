import React, { useEffect, useState } from "react";
import { useRouter } from "expo-router";
import { View, Text, TouchableOpacity, ActivityIndicator, TextInput, Image, RadioButton, Button } from "react-native";
import styles from "./ViecLam.style";
import { COLORS, icons } from "../../constants";
import ViecLamCard from "../common/cards/ViecLam/ViecLamCard";
import axios from "axios";
import Client from "../../apis/Client";

const ViecLam = () => {
    const router = useRouter();
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [keyword, setKeyword] = useState('');

    const fetchData = async () => {
        setIsLoading(true);
        try {
            const response = await Client.getJob();
            setData(response.data.data);
            setIsLoading(false);
        } catch (error) {
            alert('Something went wrong')
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSearch = async () => {
        setKeyword("");
        try {
            const response = await Client.searchJob(keyword);
            // const response = await axios.get('https://ptjobs.000webhostapp.com/api/tim-kiem-viec-lam', {
            //     params: {
            //         keyword: keyword
            //     }
            // });
            if (response.data.status === "success") {
                setData(response.data.data);
            } else {
                fetchData();
            }
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <View style={styles.container}>
            <View style={styles.searchContainer}>
                <View style={styles.searchWrapper}>
                    <TextInput
                        styles={styles.searchInput}
                        placeholder="Nhập tiêu đề / ngành nghề / địa chỉ / thời gian"
                        value={keyword}
                        onChangeText={text => setKeyword(text)}
                    />
                </View>

                <TouchableOpacity style={styles.searchBtn} onPress={handleSearch}>
                    <Image
                        source={icons.search}
                        resizeMode="contain"
                        style={styles.searchBtnImage}
                    />
                </TouchableOpacity>
            </View>
            <View style={styles.header}>
                {/* <Text style={styles.headerTitle}>Việc làm mới</Text> */}
            </View>

            <View style={styles.cardsContainer}>
                {isLoading ? (
                    <ActivityIndicator size='large' color={COLORS.primary} />
                ) : (

                    data ?
                        data?.map((job) => (
                            <ViecLamCard
                                job={job}
                                key={job.id}
                                handleNavigate={() => router.push(`/chi-tiet-viec-lam/${job.id}`)}
                            />
                        ))
                        :
                        <View>
                            <Text>Không tìm thấy kết quả</Text>
                        </View>
                )}
            </View>
        </View>
    );
};

export default ViecLam;
